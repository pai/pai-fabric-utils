from invoke import Responder
from fabric import Connection, Config
from ....constants import *


def install_docker(host, port, user, password):
    sudopass = Responder(
        pattern=r'\[sudo\] password for {}:'.format(user),
        response='{}\n'.format(password))
    config = Config(
        overrides={'user': user, 'port': port, 'connect_kwargs': {'password': password}})
    commands = [
        'sudo wget -O /tmp/{0} {1}'.format('pfu_install_docker_debian_stretch.sh', INSTALL_DOCKER_DEBIAN_STRETCH_SH),
        'sudo bash /tmp/{0}'.format('pfu_install_docker_debian_stretch.sh', INSTALL_DOCKER_DEBIAN_STRETCH_SH),
        'sudo wget -O /etc/docker/daemon.json {}'.format(DOCKER_DAEMON_JSON)
    ]
    result = 1

    for command in commands:
        result = Connection(host, config=config).run(command, pty=True, watchers=[sudopass])
        print(result)

    return result
