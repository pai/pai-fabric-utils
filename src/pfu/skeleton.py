#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This is a skeleton file that can serve as a starting point for a Python
console script. To run this script uncomment the following lines in the
[options.entry_points] section in setup.cfg:

    console_scripts =
         pfu = pfu.skeleton:run

Then run `python setup.py predicate` which will execute the command `predicate`
inside your current environment.
Besides console scripts, the header (i.e. until _logger...) of this file can
also be used as template for Python modules.

Note: This skeleton file can be safely removed if not needed!
"""

import argparse
import sys
import logging

from pfu import __version__

from .functions import install, reboot


__author__ = "pai"
__copyright__ = "pai"
__license__ = "mit"

_logger = logging.getLogger(__name__)


def predicate(do, what, where, user):
    """predicate function

    Args:
	  do (str): string
	  what (str): string
	  where (str): string

    Returns:
      int: exit code
    """

    if do == 'install':
        return install(what, where, user)
    if do == 'reboot':
        return reboot(what, where, user)
    else:
        print('Unknown "{}"'.format(do))
        return 1


def parse_args(args):
    """Parse command line parameters

    Args:
      args ([str]): command line parameters as list of strings

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(
        description="pai fabric utils")
    parser.add_argument(
        '--version',
        action='version',
        version='pfu {ver}'.format(ver=__version__))
    parser.add_argument(
        dest="do",
        help="predicate",
        type=str,
        metavar="PREDICATE")
    parser.add_argument(
        dest="what",
        help="what?",
        type=str,
        metavar="WHAT")
    parser.add_argument(
        dest="where",
        help="where?",
        type=str,
        metavar="WHERE")
		
    parser.add_argument(
        '-u',
        '--user',
        dest="user",
        help="ssh user",
        type=str,
		required=True)

    parser.add_argument(
        '-v',
        '--verbose',
        dest="loglevel",
        help="set loglevel to INFO",
        action='store_const',
        const=logging.INFO)
    parser.add_argument(
        '-vv',
        '--very-verbose',
        dest="loglevel",
        help="set loglevel to DEBUG",
        action='store_const',
        const=logging.DEBUG)
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(level=loglevel, stream=sys.stdout,
                        format=logformat, datefmt="%Y-%m-%d %H:%M:%S")


def main(args):
    """Main entry point allowing external calls

    Args:
      args ([str]): command line parameter list
    """
    args = parse_args(args)
    setup_logging(args.loglevel)
    _logger.debug("Starting crazy calculations...")
    print("The {} {} on {} exit code is {}".format(args.do, args.what, args.where, predicate(args.do, args.what, args.where, args.user)))
    _logger.info("Script ends here")


def run():
    """Entry point for console_scripts
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    run()
