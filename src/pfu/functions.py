import getpass
from urllib import request

from .constants import *
from .commands import get_linux_release_name
from .install.debian.stretch.commands import install_docker as install_docker_c
from .reboot.debian.stretch.commands import reboot as reboot_c


def run(f, what, where, user):
    hosts = list(map(lambda x: x.replace(' ', ''), where.split(',')))
    print('install {} on {}'.format(what, hosts))

    errors = 0

    for host in hosts:
        port = SSH_PORT
        sudo_pass = getpass.getpass('What\'s {} password? '.format(user))

        output_lines = get_linux_release_name(host, port, user, sudo_pass).stdout.split("\r\n")
        linux_release_name = list(filter(lambda x: x != '', output_lines))[0]
        print('linux version: "{}"'.format(linux_release_name))

        if linux_release_name == LINUX_RELEASE_NAME:
            print('-- Running {} {} on {}:{}\n'.format(f, what, host, port))

            f(host, port, user, sudo_pass)

        else:
            errors += 1
            print('Host {} distro is {} and not {}'.format(host, linux_release_name, LINUX_RELEASE_NAME))

    return errors


def install(what, where, user):
    if what == 'docker':
        return run(install_docker_c, what, where, user)
    print('Unknown install "{}"'.format(what))
    return 1

def reboot(what, where, user):
    if what == 'host':
        return run(reboot_c, what, where, user)
    print('Unknown reboot "{}"'.format(what))
    return 1
