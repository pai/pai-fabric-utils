from invoke import Responder
from fabric import Connection, Config


def get_linux_release_name(host, port, user, password):
    sudopass = Responder(
        pattern=r'\[sudo\] password for {}:'.format(user),
        response='{}\n'.format(password))
    config = Config(
        overrides={'user': user, 'port': port, 'connect_kwargs': {'password': password}})
    commands = [
        'lsb_release -cs'
    ]
    result = None

    for command in commands:
        result = Connection(host, config=config).run(command, pty=True, watchers=[sudopass])
        print('Result:', result)

    return result
