from invoke import Responder
from fabric import Connection, Config
from ....constants import *


def reboot(host, port, user, password):
    sudopass = Responder(
        pattern=r'\[sudo\] password for {}:'.format(user),
        response='{}\n'.format(password))
    config = Config(
        overrides={'user': user, 'port': port, 'connect_kwargs': {'password': password}})
    commands = [
        'sudo reboot'
    ]
    result = 1

    for command in commands:
        result = Connection(host, config=config).run(command, pty=True, watchers=[sudopass])
        print(result)

    return result
